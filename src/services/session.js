export default {
  get (key, defaultVal = null) {
    return window.sessionStorage.getItem(key) || defaultVal
  },

  set (key, val) {
    return window.sessionStorage.setItem(key, val)
  },

  remove (key) {
    return window.sessionStorage.removeItem(key)
  }
}
