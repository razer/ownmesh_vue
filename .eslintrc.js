module.exports = {
  extends: [
    'plugin:vue/recommended'
  ],
  // rules: {
  //   'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  //   'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  // },
  rules: {
    'vue/name-property-casing': 'off',
    'vue/prop-name-casing': 'off',
    'no-console': 'off',
    'max-len': ['error', { 'code': 100 }],
    'no-unused-vars': ['error', { 'argsIgnorePattern': '[uU]nused' }]
  },
  // parserOptions: {
  //   "parser": "babel-eslint",
  //   "sourceType": "module"
  // }
}